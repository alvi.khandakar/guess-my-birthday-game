from random import randint

# NEWLY Permitted functions : for and range
# Permitted functions : if print input exit int randint

#######         Old code
# name = input("Hello user, what is your name?: ")
# guess_year = randint(1924, 2004)
# guess_month = randint(1, 12)

# print('Guess 1:', name, ' were you born in', guess_month, '/', guess_year)


# answer = input('Yes or No: ').lower()

# if answer == 'yes':
#     print('I knew it')
#     exit()
# else:
#     print('Welp, let me try again')

name = input("Hello user, what is your name?: ")

tries = 0
for tries in range(0, 5):
    tries +=1
    guess_year = randint(1924, 2004)
    guess_month = randint(1, 12)
    print('Guess', tries, ':', 'Hey,', name, ', were you born in', guess_month, '/', guess_year)
    answer = input('Yes or No: ').lower()
    if answer == 'yes':
        print('I knew it!')
        exit()
    elif answer != 'yes' and tries == 5:
        print('I have other things to do, good bye')
        exit()
    else:
        print('Drat, let me try again')
    
